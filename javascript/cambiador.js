function changeColor() {
  var colorInput = document.getElementById("colorInput");
  var colorBox = document.querySelector(".color-box");
  
  var colorName = colorInput.value.toLowerCase();

  var colorMap = {
    "rojo": "#ff0000",
    "verde": "#00ff00",
    "azul": "#0000ff",
    "blanco": "#ffffff",
    "negro": "#000000",
    "amarillo": "#ffff00"
  };

  if (colorMap.hasOwnProperty(colorName)) {
    var color = colorMap[colorName];
    colorBox.style.backgroundColor = color;

    if (colorName === "rojo") {
      colorBox.style.background = "linear-gradient(to right, #ff0000, #ff0080, #ff00ff, #ff0080, #ff0000, #ff8000)";
    } else if (colorName === "verde") {
      colorBox.style.background = "linear-gradient(to right, #00ff00, #80ff80, #ffff00)";
    } else if (colorName === "azul") {
      colorBox.style.background = "linear-gradient(to left, #00ffff, #0000ff, #8000ff, #ff00ff)";
    } else if (colorName === "blanco") {
      colorBox.style.background = "linear-gradient(to right, #ffffff, #000000)";
    } else if (colorName === "negro") {
      colorBox.style.background = "linear-gradient(to right, #000000, #ffffff)";
    } else if (colorName === "amarillo") {
      colorBox.style.background = "linear-gradient(to right, #00ff00, #ffff00)";
    }
  } else {
    alert("Color desconocido");
  }
  
  colorInput.value = "";
}
